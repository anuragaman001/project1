const webdriver = require('selenium-webdriver');
const chrome = require('selenium-webdriver/chrome');
const chromedriver = require('chromedriver');

const By = webdriver.By;

async function checkIfLoginIsWorking() {
    // launch driver
    const driver = new webdriver.Builder().forBrowser('chrome').build();

    // launch browser
    await driver.get('http://localhost:4200');

    // enter email
    await driver.findElement(By.name('email')).sendKeys('anurag13@gmail.com');

    // enter password
    await driver.findElement(By.name('password')).sendKeys('anurag');

    // click on the login button
    await driver.findElement(By.name('login')).click();

    // expect an alert withe message 'welcome'
    await driver.switchTo().alert().then(alert =>  {
        console.log(alert.getText());
        driver.quit();
    })
}


// checkIfEmailIsNotEmpty();
// checkIfPasswordIsNotEmpty();
checkIfLoginIsWorking();